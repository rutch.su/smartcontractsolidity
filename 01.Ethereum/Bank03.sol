// SPDX-License-Identifier: MIT
pragma solidity  ^0.8.0;

struct Manager{
    string _name;
    string _company;
    address _address;
}


contract Bank {

    enum State {Open,Close}

    Manager public manager;
    State public _isBank = State.Open;
    
    constructor(string memory name, string memory company){
        manager._name = name;
        manager._company = company;
        manager._address = msg.sender;
    }

    modifier isManager{
        require(msg.sender == manager._address, "Unauthorized");
        _;
    }

    function getCompanyManager() public view isManager returns(string memory company){
        return manager._company;
    }

    function getBankState() public view returns (State){
        return _isBank;
    }
}