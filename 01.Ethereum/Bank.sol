// SPDX-License-Identifier: MIT
pragma solidity  ^0.8.0;

contract Bank {
    /*
    address public caller;


    function getAddress() public returns(address){
        caller = msg.sender;
        return caller;
    }

    */

    /* 
    
    mapping (key => value) name 
    
    msg.sender >>> address on MetaMask
    
    
    */
    
    mapping(address => uint) _balances;

    function deposit(uint amount) public {
        address myAddress = msg.sender;
        _balances[myAddress] += amount;
    }

    function withdraw(uint amount) public{
        _balances[msg.sender] -= amount;
    }

    function getBalance() public view returns(uint balance){
        return _balances[msg.sender];
    }

}
