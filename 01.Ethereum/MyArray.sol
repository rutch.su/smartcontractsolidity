// SPDX-License-Identifier: MIT
pragma solidity  ^0.8.0;

contract MyArray{

    uint[2] arrayMember = [1,2];

    // Dynamic array
    string[] arrayName = ["A","B"];

    function getLengthArrayMember() public view returns(uint length){
        return arrayMember.length;
    }

    function getName(uint index) public view returns(string memory name){
        return arrayName[index];
    }

    function addName(string memory name) public {
        arrayName.push(name);
    }

    function editMemory() public{
        string[] memory a = arrayName;
        a[0]="z";
    }

    function editStorage() public{
        string[] storage b = arrayName;
        b[0]="y";

    }

}