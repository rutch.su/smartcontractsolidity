import json
from web3 import Web3
import web3
infura_url = "https://rinkeby.infura.io/v3/2b2764266ce346afb072087eee4b66f1"
w3 = Web3(Web3.HTTPProvider(infura_url))
abi = json.loads('[{"inputs":[],"name":"deposit","outputs":[],"stateMutability":"payable","type":"function"},{"inputs":[],"name":"getAddressContract","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getBalance","outputs":[{"internalType":"uint256","name":"balance","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getTotalSupply","outputs":[{"internalType":"uint256","name":"totalSupply","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"withdraw","outputs":[],"stateMutability":"nonpayable","type":"function"}]')
address = "0x60815d056e5365C0a7349251A3Df84d8B00a78E0"

print("is connect api: "  + str(w3.isConnected()))

contract = w3.eth.contract(address = address,abi = abi)
totalBalance = contract.functions.getTotalSupply().call()
print(totalBalance)

balance = w3.eth.getBalance("0x915607Aa45d082B6be7051866DE48Af70786b01d")
print(w3.fromWei(balance, "ether"))




