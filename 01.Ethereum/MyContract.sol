// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/*
    1. import file sol
    2. import path git

    form link https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/utils/math/SafeMath.sol
    to
    import "github.com/OpenZeppelin/openzeppelin-contracts/contracts/utils/math/SafeMath.sol"

*/
contract MyContract {
    
    uint _balance ;
    /*
        if import SafeMath

        using SafeMath for uint;  >> extension method

        
    */

    function deposit(uint amount) public  {
        /*
            if import SafeMath
            _balance = _balance.add(amount);

        */
        _balance += amount;
    }

    function withdraw(uint amount) public {
        // require false return string
        require(amount <= _balance, "balance is not enough");
        _balance -= amount;
    }

    function getBalance() public view returns(uint balance){
        return _balance;
    }

    function getVersionContract() public pure returns(string memory){
        return "0.1.0";
    }

}