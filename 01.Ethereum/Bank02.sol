// SPDX-License-Identifier: MIT
pragma solidity  ^0.8.0;

contract Bank {

    /*
        msg.value get coin on metamask  (payable) 
    */

    mapping(address => uint) _balances;
    uint _totalSupply ;

    function deposit() public payable {
        _balances[msg.sender] += msg.value;
        _totalSupply += msg.value;
    }

    function withdraw(uint amount) public {
        /* 
        
        transfer from smart contract to sender

        payable(msg.sender).transfer(amount)

        */

        require(amount <= _balances[msg.sender], "Balance is not enough");
        payable(msg.sender).transfer(amount);
        _balances[msg.sender] -= amount;
        _totalSupply -= amount;
    }

    function getBalance() public view returns(uint balance){
        return _balances[msg.sender];
    }

    function getTotalSupply() public view returns(uint totalSupply){
        // return _totalSupply;
        return address(this).balance;
    }

    function getAddressContract() public view returns(address){
        //  address contract
        return address(this);
    }

}