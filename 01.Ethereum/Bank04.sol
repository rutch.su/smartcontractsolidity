// SPDX-License-Identifier: MIT
pragma solidity  ^0.8.0;

contract Bank{

    mapping(address => uint) _balance;
    event Deposit(address indexed owner, uint amount);
    event Withdraw(address indexed owner, uint amount);

    function deposit() public payable{
        require(msg.value > 0, "balance is zero");
        _balance[msg.sender] += msg.value;
        emit Deposit(msg.sender, msg.value);

    }

    function withdraw(uint amount) public {
        uint amountE = 1000000000000000000 * amount ;
        require(amount > 0 && amountE <= _balance[msg.sender],"not enough money");
        payable(msg.sender).transfer(amountE);
        _balance[msg.sender] -= amountE;
        emit Withdraw(msg.sender, amountE);
    }

    function getBalance() public view returns(uint){
        uint balanceE = _balance[msg.sender] / 1000000000000000000;
        return balanceE;
    }
}