// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

interface IERC20 {

    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    event Approval(address indexed _owner, address indexed _spender, uint256 _value);

    function name() external view returns (string memory);
    function symbol() external view returns (string memory);
    function decimals() external view returns (uint8);
    function totalSupply() external view returns (uint256);
    function balanceOf(address _owner) external view returns (uint256 balance);
    function transfer(address _to, uint256 _value) external returns (bool success);
    function transferFrom(address _from, address _to, uint256 _value) external returns (bool success);
    function approve(address _spender, uint256 _value) external returns (bool success);
    function allowance(address _owner, address _spender) external view returns (uint256 remaining);
    
}

abstract contract ERC20 is IERC20{
    string _name;
    string _symbol;
    uint _totalSupply;

    mapping(address => uint) _balances;
    mapping(address => mapping(address => uint)) _allowances;

    constructor(string memory name_, string memory symbol_){
        _name = name_;
        _symbol = symbol_;
    }

    function name() public override view returns (string memory){
        return _name;
    }

    function symbol() public override view returns (string memory){
        return _symbol;
    }

    function decimals() public override pure returns (uint8){
        return 0;
    }

    function totalSupply() public override view returns (uint256){
        return _totalSupply;
    }

    function balanceOf(address _owner) public override view returns (uint256 balance){
        return _balances[_owner];
    }

    function transfer(address _to, uint256 _value) public override returns (bool success){
        _transfer(msg.sender, _to, _value);
        return true;
    }

    function approve(address _spender, uint256 _value) public override returns (bool success){
        _approve(msg.sender, _spender, _value);
        return true;
    }

    function allowance(address _owner, address _spender) public override view returns (uint256 remaining){
        return _allowances[_owner][_spender];
    }
 
    function transferFrom(address _from, address _to, uint256 _value) public override returns (bool success){
        if( _from !=  msg.sender ){
           uint allowancesAmount = _allowances[_from][msg.sender];
           require(_value <= allowancesAmount, "transfer amount from exceeds allowance");
           _approve(_from, msg.sender, allowancesAmount - _value );
        }

        _transfer(msg.sender, _to, _value);
        return true;
    }

    /* Private Function */

    function _transfer(address from, address to, uint amount) internal {
        
        require(from != address(0),"transfer from zero address");
        require(to != address(0), "transfer to zero address");
        require(amount <= _balances[from], "transfer amount from exceeds balance");
        
        _balances[from] -= amount;
        _balances[to] += amount;

        emit Transfer(from,to,amount);
    }

    function _approve(address owner, address spender, uint amount) internal{
        
        require(owner != address(0), "Approval from zero address");
        require(spender != address(0), "Approval spender zero address");

        _allowances[owner][spender] = amount;
       emit Approval(owner, spender, amount); 
    }

    function _mint(address to, uint amount) internal {
        require(to != address(0), "mint to zero address");
        _balances[to] += amount;
        _totalSupply += amount;

        emit Transfer(address(0),to,amount);
    }

    function _burn(address from, uint amount) internal {
        require(from != address(0), "burn from zero address");
        require(amount > 0, "amount is not zero");
        require(amount <= _balances[from] , "burn amount exceeds balance");

        _balances[from] -= amount;
        _totalSupply -= amount;
        emit Transfer(from,address(0),amount);
    }
}

contract ADC is ERC20{

    constructor() ERC20("ANTI DOI Coin","ADC"){

    }

    function deposit() public payable{
        require(msg.value > 0 , "amount is zero");
        _mint(msg.sender, msg.value);
    }

    function withdraw(uint amount) public {
        require(amount > 0 && amount <= _balances[msg.sender], "withdraw amount" );
        payable(msg.sender).transfer(amount);
        _burn(msg.sender, amount);
    }
}

